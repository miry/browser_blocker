function ad_blocker() {
  var player = document.querySelector("#movie_player");

  function click_button(selector) {
    const buttons = document.querySelectorAll(selector);
    for (const button of buttons) {
      console.log("[Browser blocker] Found Skip button:", button);
      button.click();
    }
  }

  // Ads above the chat and recomendation
  function remove_ads() {
    var remove_by_selectors = [
      // ".video-ads",
      // ".ytd-action-companion-ad-renderer",
      ".ytp-ad-module",
      "[is-shorts]",
      ".ytd-shorts",
      "#shorts-player",
      "#chat-container",
      "#player-ads",
      ".ytp-ad-button",
      ".ytp-ad-visit-advertiser-button",
      "ytd-ad-slot-renderer",
      "ytd-engagement-panel-section-list-renderer[target-id=engagement-panel-ads]",
      "ytd-merch-shelf-renderer",
      "ytd-promoted-sparkles-web-renderer"
    ];
    for (const selector of remove_by_selectors) {
      const element = document.querySelector(selector);
      if (element) {
        console.log("[Browser blocker] Found ad:", element);
        element.remove();
      }
    }
  }

  function skip_trial() {
    let elements = document.querySelectorAll(".ytd-popup-container #main.ytd-mealbar-promo-renderer #dismiss-button #button");
    for (const node of elements) {
      if (node.textContent == "Skip trial") {
        node.click();
        node.remove();
      }
    }
  }

  var intervalID;
  var vary = function () {
    try {
      remove_ads();
      click_button(".ytp-ad-skip-button-slot");
      click_button(".ytp-ad-skip-button-modern");
      click_button(".ytp-ad-skip-button-text");
      click_button(".ytp-ad-overlay-close-button");
      skip_trial();

      if (player && player.classList.contains("ad-showing")) {
        console.log("[Browser blocker] Playing ad", player.classList);
        // player.classList.remove("ad-showing");
        // player.classList.remove("ad-interrupting");
        // console.log("[Browser blocker] Should stop!");
      }
    } catch (e) {
      console.error("[Browser blocker] [ERROR]: ", e.message);
      console.error(e);
    }
  };

  intervalID = setInterval(vary, 500);
}

function main() {
  console.log("[Browser blocker] Youtube adapter loading....");
  ad_blocker();
}

main();
