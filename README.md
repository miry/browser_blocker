# Browser Blocker

This is just an example to show how easy it is to build a basic ad blocker nowadays.
While it may not offer any advantages over famous solutions, 
the key benefit is peace of mind—you know there are no hidden trackers or unintended data leaks from third-party extensions.

## Usage

### Addon

Download addon from Releases page https://codeberg.org/miry/browser_blocker/releases.
In browser load the addon from the downloaded file.

### Host

Sync `/etc/hosts`:

```
curl https://codeberg.org/miry/browser_blocker/raw/branch/master/sync.sh | bash -x
```

### PiHole

PiHole enables synchronization of adlists for enhanced ad blocking. To add a new adlist, use the following URL:
`https://codeberg.org/miry/browser_blocker/raw/branch/master/hosts.raw`

Simply paste this URL into your PiHole's adlist configuration to integrate the latest ad blocking rules.

## References

- https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Modify_a_web_page
- https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/content_scripts

## Sources

- https://pgl.yoyo.org/as/serverlist.php?showintro=0;hostformat=hosts
- https://pgl.yoyo.org/as/serverlist.php?hostformat=plain;showintro=0
- https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

# Firefox Addon

* https://codeberg.org/miry/browser_blocker/releases/

## Settings

You can specify a URL to custom list of filtering rules in Addon preference page: `about:addons`.

## Release

### Automation

1. Generate addon

```shell
$ rake release
```

### Manual steps

1. Update version in all project.
1. Create release commit: `git commit -S -a -m "Release v0.5.14"`
1. Archive:
   ```shell
   $ zip -r build/browser_blocker-0.5.14.xpi manifest.json icons/icon-*png yblocker.js tblocker.js blocker_extension.js icons/*jpg options.*
   ```
1. Create git tag and a Codeberg Release: `git tag -s v0.5.14`

### Common steps

1. Visit https://addons.mozilla.org/en-us/developers/addon/browser_blocker/versions
1. Click on link Upload new version
1. Download signed version from https://addons.mozilla.org/en-us/developers/addon/browser_blocker/versions
