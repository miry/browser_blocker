#!/usr/bin/env ruby

require "json"

class Rule
  attr_reader :raw, :protocol, :host, :port, :uri
  attr_reader :domain_chunks, :domain_revers

  PATTERN = /^(?<protocol>[^:]*):\/\/(?<host>[^:\/]*)(:(?<port>:\d+))?(?<uri>.*)/
  def initialize(str)
    @raw = str
    @parts = parse(str)

    @protocol = @parts["protocol"]
    @host = @parts["host"]
    @port = @parts["port"]
    @uri = @parts["uri"]


    @domain_chunks = split_domain(@host)
    @domain_revers = @domain_chunks.join(".") + @uri
  end

  def parse(str)
    PATTERN.match(str).named_captures
  end

  def split_domain(host)
    host.split(".").reverse
  end

  def to_s
    @raw
  end
end

class Hostname
  attr_reader :domain_revers
  
  def initialize(input)
    @raw = input
    @domain_chunks = @raw.split(".").reverse
    @domain_revers = @domain_chunks.join(".")
  end
  
  def to_s
    @raw
  end
end

def sort_browser
  brwoser_json = "browser.json"
  file = File.read(brwoser_json)

  rules = JSON.parse(file)

  uris = rules.map {|r| Rule.new r}
  uris.sort! {|a,b| a.domain_revers <=> b.domain_revers }
  uris = uris.map(&:to_s).uniq

  File.write(brwoser_json, JSON.pretty_generate(uris) + "\n")
end

def sort_hosts
  hosts_file = "hosts"
  file = File.read(hosts_file).split.map(&:strip)
  file.uniq!
  file = file.map {|host| Hostname.new(host) }
  file.sort! {|a,b| a.domain_revers <=> b.domain_revers }
  File.write(hosts_file, file.join("\n") + "\n")
  File.write(hosts_file+".raw", file.map{|s| "0.0.0.0 #{s}" }.join("\n") + "\n")
end

sort_browser
sort_hosts
