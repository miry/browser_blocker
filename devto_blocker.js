function waitForElm(selector) {
  return new Promise(resolve => {
      if (document.querySelector(selector)) {
          return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver(mutations => {
          if (document.querySelector(selector)) {
              resolve(document.querySelector(selector));
              observer.disconnect();
          }
      });

      observer.observe(document.body, {
          childList: true,
          subtree: true
      });
  });
}

function devto_ad_blocker() {
  const spans = document.querySelectorAll('.crayons-card[data-category-click="click"]');
  for (let i = 0; i < spans.length; i++) {
    spans[i].remove();
  }
}

function main() {
  console.log("[Browser blocker] DevTo adapter loading....");

  waitForElm('main#main-content').then((page_body_element) => {
    const callback = (mutationList, observer) => {
      for (const mutation of mutationList) {
        if (mutation.type === 'childList') {
          devto_ad_blocker();
        }
      }
    };
    const observer = new MutationObserver(callback);
    observer.observe(page_body_element, {childList: true, subtree: true });
  });
}

main();
