# frozen_string_literal: true

require "rake/phony"
require "json"

# BIN_PATH = ""
# OUTPUT_PATH = ""
#
# desc "Build firefox extension in #{OUTPUT_PATH}"
# task build: BIN_PATH
#
# task BIN_PATH => [OUTPUT_PATH] do |t|
#   sh "crystal build --release --no-debug --error-trace -o #{t.name} src/cli.cr"
# end

BUILD_PATH = "build"

desc "Release a new addon"
task release: ["release:bump", "release:build", "release:commit"] do
  puts "Open in browser: https://addons.mozilla.org/en-us/developers/addon/browser_blocker/versions"
end

namespace :release do
  desc "Bump version"
  task :bump do
    manifest_path = "manifest.json"
    manifest_content = File.read(manifest_path)

    current_version = JSON.parse(manifest_content)["version"]
    if current_version.nil? || current_version == ""
      raise "Could not find version in `#{manifest_path}`"
    end
    ver = current_version.split(".").map(&:to_i)
    ver[-1] += 1
    new_version = ver.join(".")

    # Update manifest file
    manifest_content.gsub!(/#{current_version}/, new_version)
    File.write(manifest_path, manifest_content)

    # Update Readme file
    readme_path = "README.md"
    redme_content = File.read(readme_path).gsub(/#{current_version}/, new_version)
    File.write(readme_path, redme_content)
  end

  desc "Build Firefox addon"
  task :build do
    version = extract_version
    sh "zip -r #{BUILD_PATH}/browser_blocker-#{version}.xpi manifest.json icons/icon-*png lblocker.js yblocker.js tblocker.js devto_blocker.js blocker_extension.js icons/*jpg options.*"
  end

  desc "Commit release changes"
  task :commit do
    version = extract_version
    sh "git commit -S -a -m \"Release v#{version}\""
    sh "git tag -s \"v#{version}\" -m \"v#{version}\""
  end
end

def extract_version
  manifest_path = "manifest.json"
  result = JSON.load_file(manifest_path)["version"]
  if result.nil? || result == ""
    raise "Could not find version in `#{manifest_path}`"
  end
  result
end
