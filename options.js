const input_selector = "#urls_source";
const default_url_source = "https://codeberg.org/miry/browser_blocker/raw/branch/master/browser.json";

function saveOptions(e) {
  e.preventDefault();
  browser.storage.sync.set({
    urls_source: document.querySelector(input_selector).value
  });
}

function restoreOptions() {
  function setCurrentChoice(result) {
    document.querySelector(input_selector).value = result.urls_source || default_url_source;
  }

  function onError(error) {
    console.log(`[Browser blocker] [ERROR]: ${error}`);
  }

  let getting = browser.storage.sync.get("urls_source");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
