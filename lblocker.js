function linkedin_ad_blocker() {
  function remove_sidebar() {
    const sidebar = document.querySelector("aside.scaffold-layout__aside");
    if (sidebar) {
      console.log("[Browser blocker] Remove sidebar!");
      sidebar.remove();
    }
  }
  remove_sidebar();

  function remove_ads(element) {
    console.log("[Browser blocker] Found an advertisment article:", element);
    let parent = element.closest('div > [data-finite-scroll-hotkey-item]')
    if (!parent) {
      parent = element.closest('#fie-impression-container');
    }
    parent = parent.parentElement;
    parent.remove();
  }

  // Remove messages with Promoted text
  let elements = document.querySelectorAll('[aria-label="Promoted"]');
  for (var i = 0; i < elements.length; i++) {
    remove_ads(elements[i]);
  }

  let spans = document.querySelectorAll('span[aria-hidden="true"]');
  let span = null;
  for (var i = 0; i < spans.length; i++) {
    span = spans[i];
    if (span.childElementCount == 0 && span.textContent.includes("Promoted")) {
      remove_ads(span);
    }
  }

  // Remove messages with Suggested text
  spans = document.querySelectorAll('.update-components-header span.update-components-header__text-view');
  for (var i = 0; i < spans.length; i++) {
    span = spans[i];
    if (span.childElementCount == 0 && span.textContent.includes("Suggested")) {
      remove_ads(span);
    }
  }
}

function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function main() {
  console.log("[Browser blocker] Linkedin adapter loading....");
  linkedin_ad_blocker();

  waitForElm("#voyager-feed").then((timeline_element) => {
    const callback = (mutationList, observer) => {
      for (const mutation of mutationList) {
        if (mutation.type === 'childList') {
          linkedin_ad_blocker();
        }
      }
    };
    const observer = new MutationObserver(callback);
    observer.observe(timeline_element, { childList: true, subtree: true });
  });
}

main();
