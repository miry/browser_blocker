const default_url_source =
  "https://codeberg.org/miry/browser_blocker/raw/branch/master/browser.json";
var urls_source = default_url_source;

function httpGet(theUrl, callback) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, true);
  xmlHttp.onload = function (e) {
    if (xmlHttp.readyState === 4) {
      if (xmlHttp.status === 200) {
        callback(JSON.parse(xmlHttp.responseText));
      } else {
        console.error(xmlHttp.statusText);
      }
    }
  };
  xmlHttp.send(null);
}

function updateFilters(urls) {
  try {
    var b = (function () {
      if (typeof browser !== "undefined") {
        return browser;
      }
      return chrome;
    })();

    const callback = function (details) {
      console.log(`[Browser blocker] Block: ${details.url}`);
      return { cancel: true };
    };
    const opt_extraInfoSpec = ["blocking"];
    const filter = { urls: urls };

    console.log(
      `[Browser blocker] Loading list ${urls.length} with last ${urls[urls.length - 1]}`,
    );
    console.log(
      b.webRequest.onBeforeRequest.addListener(
        callback,
        filter,
        opt_extraInfoSpec,
      ),
    );
  } catch (err) {
    console.error(err);
  }
}

// Remove ip from youtube
// function removeIPfromYoutube(urls) {
//   var b = (function () {
//     if (typeof browser !== 'undefined') {
//       return browser
//     }
//     return chrome;
//   })();

//   var redirect = function (details) { console.log("addon debug"); console.log(details); return { cancel: true }; };
//   var opt_extraInfoSpec = ["blocking"];
//   var filter = {
//     urls: [
//       "https://*.googlevideo.com/videoplayback*"
//     ]
//   };

//   b.webRequest.onBeforeSendHeaders.addListener(callback, filter, opt_extraInfoSpec);
// }

function refresh(source) {
  let date = new Date();
  let refresh = date.getTime();
  httpGet(source + "?" + refresh, updateFilters);
}

function blocker_extension_init() {
  browser.storage.sync.get("urls_source").then(function (result) {
    urls_source = result.urls_source || default_url_source;
    console.log(`[Browser blocker] Read urls source: ${urls_source}`);
    refresh(urls_source);
  });
}

function static_blocker_url() {
  const callback = function (details) {
    console.log("yt_block_url:");
    console.log(`[Browser blocker] yt_block_url Block: ${details.url}`);
    return { cancel: true };
  };
  const opt_extraInfoSpec = ["blocking"];
  const filter = {
    urls: [
      "*://*.ads.youtube.com/*",
      "*://*.youtube-nocookie.com/youtubei/v1/log_event*",
      "*://*.youtube-nocookie.com/youtubei/v1/log_interaction*",
      "*://*.youtube-nocookie.com/youtubei/v1/player/ad_break*",
      "*://*.youtube.com/*action=video_to_ad*",
      "*://*.youtube.com/csi_204*",
      "*://*.youtube.com/getDatasyncIdsEndpoint*",
      "*://*.youtube.com/pagead/*",
      "*://*.youtube.com/youtubei/v1/att/get?*",
      "*://*.youtube.com/ad_companion*",
      "*://*.youtube.com/annotations_invideo*",
      "*://*.youtube.com/api/stats*",
      "*://*.youtube.com/api/stats/*",
      "*://*.youtube.com/generate_204*",
      "*://*.youtube.com/get_midroll_info*",
      "*://*.youtube.com/get_video_info?*adformat=*",
      "*://*.youtube.com/live_chat_replay*",
      "*://*.youtube.com/mac_204?action_fcts=1",
      "*://*.youtube.com/pagead*",
      "*://*.youtube.com/pagead/adview*",
      "*://*.youtube.com/pagead/adview/*",
      "*://*.youtube.com/pagead/conversion*",
      "*://*.youtube.com/pcs/activeview*",
      "*://*.youtube.com/ptracking*",
      "*://*.youtube.com/s/desktop/*/jsbin/live_chat_polymer.vflset/live_chat_polymer.js",
      "*://*.youtube.com/shorts/*",
      "*://*.youtube.com/youtubei/v1/log_event*",
      "*://*.youtube.com/youtubei/v1/log_interaction*",
      "*://*.youtube.com/youtubei/v1/player/ad_break*",
      "*://*.youtube.com/yts/cssbin/www-adcompanion*.css",
      "*://*.youtube.com/yts/jsbin/*/www-adcompanion.js",
      "*://*.youtube.com/yts/jsbin/*/www-pagead-id.js",
      "*://*.youtube.com/yts/jsbin/www-pagead-id-vfltmZSl7/www-pagead-id.js",
      "*://*.adservice.google.com/*",
      "*://*.clientmetrics-pa.googleapis.com/*",
      "*://*.google-analytics.com/*",
      "*://*.google.com/gen_204*",
      "*://*.google.com/pagead/*",
      "*://*.googleadservices.com/*",
      "*://*.googleanalytics.com/*",
      "*://*.googleapis.com/youtube/v3/channels?id=UC5pVts2vIbXtij3gb-hOszw*",
      "*://*.googleoptimize.com/*",
      "*://*.googlesyndication.com/*",
      "*://*.googletagmanager.com/*",
      "*://*.googletagservices.com/*",
      "*://*.googlevideo.com/generate_204*",
      "*://*.googlevideo.com/videoplayback?*&ctier=L&*",
      "*://*.googlevideo.com/videoplayback?*&itag=244&aitags=133,134,135,136,137,160,242,243,244,247,248,278&*",
      "*://*.googlevideo.com/videoplayback?*&itag=397&*",
      "*://*.jnn-pa.googleapis.com/*",
      "*://*.mail-ads.google.com/*",
      "*://*.pagead.l.google.com/*",
      "*://*.partnerad.l.google.com/*",
      "*://*.video-stats.video.google.com/*",
      "*://*.wintricksbanner.googlepages.com/*",
      "*://*.www-google-analytics.l.google.com/*",
      "*://accounts.google.com/gsi/log*",
      "*://adservice.google.com/*",
      "*://apis.google.com/js/plusone.js",
      "*://payments.google.com/payments/v4/js/integrator.js*",
      "*://play.google.com/log*",
      "*://ssl.google-analytics.com/*",
      "*://translate.googleapis.com/element/log*",
    ],
  };

  console.log("[Browser blocker] yt_block_url Loading Youtube list");
  browser.webRequest.onBeforeRequest.addListener(
    callback,
    filter,
    opt_extraInfoSpec,
  );
}

static_blocker_url();
blocker_extension_init();
browser.browserAction.onClicked.addListener(blocker_extension_init);
