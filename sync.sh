#!/usr/bin/env bash

# Download current state of the gist. Concatenate the output of the hosts and gist
{ curl -s "https://codeberg.org/miry/browser_blocker/raw/branch/master/hosts.raw?$(date +%s)" & cat /etc/hosts ; } | sort | uniq | grep '0.0.0.0' | grep -v -e '(^\s*$\|localhost\|broadcasthost\|#)' > /tmp/hosts

# /etc/hosts should put blocked part between 2 lines of `#ADBLOCKER`
{ sed '/\#ADBLOCKER/,/\#ADBLOCKER/d' /etc/hosts;  echo '#ADBLOCKER';  cat /tmp/hosts; echo '#ADBLOCKER' ; } | sudo tee /etc/hosts
