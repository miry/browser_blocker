function twitter_ad_blocker() {
  function remove_sidebar() {
    let sidebar = document.querySelector("[data-testid=sidebarColumn]");
    if (sidebar) {
      console.log("[Browser blocker] Remove sidebar!");
      sidebar.remove();
    }
  }
  remove_sidebar();

  function remove_ads(span) {
    var parent = span.parentElement;
    while (parent.getAttribute("data-testid") != "cellInnerDiv") {
      parent = parent.parentElement;
    }
    // parent.remove();
    parent.style.visibility = "hidden";
  }

  var spans = document.getElementsByTagName("span");
  let promotted_texts = ["Promoted", "Ad"];

  for (var i = 0; i < spans.length; i++) {
    let span = spans[i];
    if (promotted_texts.includes(span.textContent)) {
      console.log("[Browser blocker] Found a promoted article!");
      remove_ads(span);
    }
  }
}

function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(mutations => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}

function main() {
  console.log("[Browser blocker] Twitter adapter loading....");
  twitter_ad_blocker();

  waitForElm("main[role='main']").then((timeline_element) => {
    const callback = (mutationList, observer) => {
      for (const mutation of mutationList) {
        if (mutation.type === 'childList') {
          twitter_ad_blocker();
        }
      }
    };
    const observer = new MutationObserver(callback);
    observer.observe(timeline_element, { childList: true, subtree: true });
  });
}

main();
